#!/bin/bash

if [ ! $# -eq 2 ]
  then
    echo "Usage: " $0 " [crate_name] [crate_version]"
fi

CRATE_NAME=$1
CRATE_VERS=$2

TMP_PROJECT_PATH=temp_project

echo Download $CRATE_NAME, version $CRATE_VERS

# Generate Cargo.lock
cargo new $TMP_PROJECT_PATH
echo $CRATE_NAME = \"$CRATE_VERS\" >> $TMP_PROJECT_PATH/Cargo.toml
cd $TMP_PROJECT_PATH && cargo metadata && cd ../

# Download dependencies
IFS=$'\n'       # make newlines the only separator
CURR_PACKAGE_NAME=""
CURR_PACKAGE_VERSION=""
for i in $(cat $TMP_PROJECT_PATH/Cargo.lock | grep "^name = \|^version = "); do
	
	if [[ $i == name* ]] ;
	then
		CURR_PACKAGE_NAME=$(echo "$i" | cut -d"\"" -f 2)
	fi

	if [[ $i == version* ]] ;
	then
		CURR_PACKAGE_VERSION=$(echo "$i" | cut -d"\"" -f 2)


		CMD=$(echo "wget -x" https://static.crates.io/crates/$CURR_PACKAGE_NAME/$CURR_PACKAGE_NAME-$CURR_PACKAGE_VERSION.crate)
		echo $CMD
		
		if [[ $CURR_PACKAGE_NAME == temp_project ]]
		then
			echo "Skipped temp package"
		else
			eval $CMD
		fi
	fi


done



# Cleanup
rm -rf $TMP_PROJECT_PATH