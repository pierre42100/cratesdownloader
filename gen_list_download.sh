#!/bin/bash

if [ $# -eq 0 ]
  then
    echo "Usage: " $0 "[Cargo.lock]"
fi

IFS=$'\n'       # make newlines the only separator
for i in $(cat $1 | grep "^\"checksum "); do

	PKG_NAME=$(echo "$i" | cut -d" " -f 2)
	PKG_VERSION=$(echo "$i" | cut -d" " -f 3)
	echo "wget -x" https://static.crates.io/crates/$PKG_NAME/$PKG_NAME-$PKG_VERSION.crate
done